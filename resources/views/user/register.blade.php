@extends('user.layouts.master')
@section('css')
    <link rel="stylesheet" href="css/recrutmint_style.css">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="fSec seven">

            <h3 class="mz1 text-center">Registration Form</h3>
            <div class="ri1">
                <h3 class="e1 text-center"></h3>
                <span class="daimond"></span>
                <p class="d1">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                    containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                    including versions of Lorem Ipsum.


                </p>
            </div>
        </div>
    </section>
    @if(session()->has('message'))
        <div class="message">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="form">

        <form id="submitForm" method="POST"  action="{{ url('/register') }}">
            {{ csrf_field() }}
            @include('user.layouts.errors')
            <div class="row">
                <div class="row seven" id="first">

                    <div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                        <input value="{{ old('name') }}"  name="name" id="name" type="text" class="validate">
                        <label for="name">First Name</label>
                        <span id="nameErr"></span>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">phone</i>
                        <input value="{{ old('phone') }}" name="phone" id="phone_number" type="tel" class="validate">
                        <label for="telephone">Telephone</label>
                        <span id="phoneErr"></span>
                    </div>


                    <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input  name="password" id="password" type="password" class="validate">
                        <label for="password">Password</label>
                        <span id="passwordErr"></span>
                    </div>

                    <div class="input-field col s6">
                        <i class="material-icons prefix">lock</i>
                        <input  name="password_confirmation" id="password_con" type="password" class="validate">
                        <label for="password_confirmation">Password Confirmation</label>
                    </div>

                    <div class="input-field col s12">
                        <i class="material-icons prefix">email</i>
                        <input value="{{ old('email') }}" name="email" id="email" type="text" class="validate">
                        <label for="email">Email</label>
                        <span id="emailErr"></span>
                    </div>



                    <div id="university_select" class="input-field col s12">
                        <select name="company" id="company" >
                                <option value="" disabled selected>Choose Company:</option>
                                @foreach($companies as $company)
                                    <option value={{ $company->id }}>{{ $company->name }}</option>
                                @endforeach
                        </select>
                    </div>


                    <button  id="subm" class="btn waves-effect waves-light" type="button" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button>

                </div>

            </div>
        </form>
    </div>
@endsection

@section('js')
    <script src="js/recrutmint_script.js"></script>
@endsection

