<html>
<head>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{ 'css/bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ asset('css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"> {{--// color #2B3137--}}


    @yield('css')

</head>
<body>

@include('user.layouts.nav')

@yield('content')

@include('user.layouts.footer')

@yield('js')

</body>
</html>