@extends('user.layouts.master')
@section('css')
    <link rel="stylesheet" href="css/login.css">
@endsection

@section('content')
    <div class="con-card">
        <form method="POST" enctype="multipart/form-data" action="{{ url('/login') }}">
            {{ csrf_field() }}

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{
                    session()->get('error')
                    }}
                </div>
            @endif
            <div class="row">

                <div id="first">
                    <h4 class="e1 text-center">Welcome</h4>
                    <span class="daimond"></span>


                    <div class="input-field col s12">
                        <i class="material-icons prefix">email</i>
                        <input value="{{ old('email') }}" name="email" id="icon_email" type="text" class="validate">
                        <label for="email">Email</label>
                    </div>

                    <div class="pass input-field col s12">
                        <i class="material-icons prefix">lock</i>
                        <input value="{{ old('password') }}" name="password" id="icon_unlock" type="password" class="validate">
                        <label for="password">Password</label>
                    </div>

                        <p>
                            <label>
                                <input value="0" type="checkbox" name="rememberme" />
                                <span>Remember me</span>
                            </label>
                        </p>

                    <button class="btn waves-effect waves-light" type="submit" name="action">Log in

                    </button>
                    <br><br>
                    <center>
                        <a href="{{ url('register') }}">Create an account
                            .</a> <br><br>

                    </center>
                </div>
            </div>
        </form>
        <div class="slide">
            <div class="slider">
                <ul class="slides">

                    <li>
                        <div class="active caption right-align">
                            <h3>Left Aligned Caption</h3>
                            <p class="light grey-text text-lighten-3">Nobody wants to miss out on important events, but sometimes, it’s hard to find the time in another city. Clocks+ lets you set alarms for your favorite cities in a simple and intuitive way.

                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="caption right-align">
                            <h3>Right Aligned Caption</h3>
                            <p class="light grey-text text-lighten-3">Nobody wants to miss out on important events, but sometimes, it’s hard to find the time in another city. Clocks+ lets you set alarms for your favorite cities in a simple and intuitive way.

                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="caption right-align">
                            <h3>This is our big Tagline!</h3>
                            <p class="light grey-text text-lighten-3">Nobody wants to miss out on important events, but sometimes, it’s hard to find the time in another city. Clocks+ lets you set alarms for your favorite cities in a simple and intuitive way.

                            </p>
                        </div>
                    </li>
                </ul>

            </div>
        </div>

        @endsection

        @section('js')
            <script src="js/login.js"></script>
@endsection


