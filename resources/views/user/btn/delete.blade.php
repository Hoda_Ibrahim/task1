<button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#delete" style="padding: 3px 12px;"><i class="fa fa-remove"></i></button>

<!-- Modal -->
<div id="delete" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="{{ aurl('users/user/'.$id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method"  value="DELETE">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete record</h4>
                </div>
                <div class="modal-body">
                    <p>Delete this record ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>

                </div>
            </div>
        </form>
    </div>
</div>