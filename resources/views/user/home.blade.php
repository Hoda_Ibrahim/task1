@extends('user.layouts.master')
@section('css')
    <link rel="stylesheet" href="css/recrutmint_style.css">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
@endsection

@section('content')
    <section>
        <div class="fSec seven">

            <h3 class="mz1 text-center">Welcome to home page</h3>
            <div class="ri1">
                <h3 class="e1 text-center"></h3>
                <span class="daimond"></span>
                <p class="d1">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                    containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                    including versions of Lorem Ipsum.


                </p>
            </div>
        </div>
    </section>


@endsection

@section('js')
@endsection

