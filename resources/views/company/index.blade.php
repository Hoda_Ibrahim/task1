@extends('admin.index')

@section('content')
    @if(session()->has('added'))
            <div class="alert alert-success">
                {{ session()->get('added') }}
            </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ $title }}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form id="formSubmit" method="POST" action="{{ aurl('companies/destroy/all') }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                {!! $dataTable->table([
                    'class' => 'table table-bordered table-hover'
                ] , true) !!}
            </form>
        </div>
        <!-- /.box-body -->
    </div>

    <!-- Modal -->
    <div id="mutiDelete" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p class="records hidden">Do U want to delete <span class="records_num"></span> records ?</p>
                    <p class="no_records hidden">2tnayel 25tar records 2wal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default no_records hidden" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default records hidden" data-dismiss="modal">No</button>
                    <input type="submit" onclick="sub()" value="Yes" class="btn btn-danger records hidden">
                </div>
            </div>

        </div>
    </div>
    @push('js')
        <script>
            delete_all();
        </script>
        {!! $dataTable->scripts() !!}
    @endpush
@endsection