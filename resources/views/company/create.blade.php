<?php

?>
@extends('admin.index')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add new Company</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{ aurl('companies/store') }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input  value="{{ old('name') }}" type="text" class="form-control" name="name" id="inputEmail3" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input value="{{ old('email') }}" type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tel" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input  value="{{ old('tel') }}" type="text" name="tel" class="form-control" id="inputEmail3" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-10">
                                <input type="text" name="address" value="{{ old('address') }}" class="form-control" id="inputPassword3" placeholder="Address">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">

                        <button type="submit" class="btn btn-info pull-right">Add <i class="fa fa-plus"></i> </button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </div>
        <!-- /.box-body -->
    </div>

@endsection