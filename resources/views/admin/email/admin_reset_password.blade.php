@component('mail::message')
#
Hello {{ $data['data']->name }}
@component('mail::button', ['url' => aurl('reset/password/'.$data['token'])])
Click here to reset your password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
