<?php

?>
@extends('admin.index')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Admin</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="box box-info">

                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="{{ aurl('admin/'.$admin->id) }}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input  value="{{ $admin->name }}" type="text" class="form-control" name="name" id="inputEmail3" placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input value="{{ $admin->email  }}" type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input  value="{{ $admin->phone }}" type="text" name="phone" class="form-control" id="inputEmail3" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select" class="col-sm-2 control-label">Country</label>
                            <div class="col-sm-10">
                                <select name="company" class="form-control">
                                    @foreach($companies as $company)
                                        <option value={{ $company->id }}>{{ $company->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">

                        <button type="submit" class="btn btn-info pull-right">Add <i class="fa fa-plus"></i> </button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </div>
        <!-- /.box-body -->
    </div>


@endsection