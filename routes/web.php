<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('register' , 'UserController@register');
Route::post('register' , 'UserController@store');

Route::group(['middleware' => 'guest'] , function () {
    Route::get('login' , 'UserController@login');
    Route::post('login' , 'UserController@loginAction');
});

Route::group(['middleware'=>'auth'] , function() {
    Route::get('home', 'UserController@index');
    Route::any('logout' , 'UserController@logout');
});

Route::group(['prefix' => 'admin' , 'namespace' => 'Admin'], function (){

    Config::set('auth.defines' , 'admin');
    Route::get('login' , 'AdminAuth@login');
    Route::get('forgot_password' , 'AdminAuth@forgot_password');
    Route::get('reset/password/{token}' , 'AdminAuth@reset_password');
    Route::post('forgot_password' , 'AdminAuth@forgot_password_post');
    Route::any('logout' , 'AdminAuth@logout');
    Route::post('login' , 'AdminAuth@loginAction');
    Route::post('reset/password/{token}' , 'AdminAuth@reset_password_post');


    Route::group(['middleware'=>'admin:admin'] , function(){
        Route::resource('admin' , 'AdminController');
        Route::get('users' , 'UserController@index');
        Route::get('admin/create' , 'AdminController@create');
        Route::get('admin/companies/create' , 'CompaniesController@create');
        Route::get('users/create' , 'UserController@create');
        Route::post('users/store' , 'UserController@store');
        Route::delete('users/destroy/all' , 'UserController@multi_delete');
        Route::delete('users/user/{id}' , 'UserController@destroy');
        Route::get('users/user/{id}/edit' , 'UserController@edit');
        Route::put('users/user/{id}' , 'UserController@update');
        Route::post('admin/store' , 'AdminController@store');
        Route::get('companies' , 'CompaniesController@index');
        Route::get('companies/create' , 'CompaniesController@create');
        Route::post('companies/store' , 'CompaniesController@store');
        Route::delete('companies/destroy/all' , 'CompaniesController@multi_delete');
        Route::delete('companies/company/{id}' , 'CompaniesController@destroy');
        Route::get('companies/company/{id}/edit' , 'CompaniesController@edit');
        Route::put('companies/company/{id}' , 'CompaniesController@update');
        Route::get('/' , function (){
            return view('admin.index');
        });
    });

});
