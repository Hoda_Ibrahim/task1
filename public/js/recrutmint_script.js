var fbutton = document.getElementById('fbuttom'),
    first = document.getElementById('first'),
    second = document.getElementById('second'),




    nameErr = document.getElementById('nameErr'),
    phoneErr = document.getElementById('phoneErr'),
    emailErr = document.getElementById('emailErr'),
    passwordErr = document.getElementById('passwordErr'),
    volunteerErr = document.getElementById('volunteerErr'),

    form = document.getElementById('test');


var nameIn = $('#name'),
    phoneIn = $('#phone_number'),
    emailIn = $('#email'),
    passwordIn = $('#password');




nameIn.blur(function () {
    var name = $('#name').val();
    // var reg = /[0-9]/g;
    // if(! reg.test(phone_number))
    // {
    //     alert('not num');
    // }
    //     else if($('#phone_number').val().match(/^\d+$/)){
    //             alert('not num');
    //             flag = 1;
    //         }

    var flag = 0;

    var character = new Array(name.length);
    var i,a , tester = false;
    for (i=0, a = name.length; i < a; i=i+1) {
        character[i] = name[i];
        if (character[i] == "1" || character[i] == "2" || character[i] == "3" ||
            character[i] == "4"|| character[i] == "5" || character[i] == "6" ||
            character[i] == "7" || character[i] == "8" ||
            character[i] == "9" || character[i] == "0"){
            tester = true;
        }
    }


    if (!name){
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "We need to know your name!";
        flag = 1;
    }
    else if(tester){
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "Name shouldn't contain numbers";
        flag = 1;
    }
    else if (name.length < 5){
        nameErr.innerHTML = 'Name should be more than 5 letters.';
        nameIn.css({'border-bottom': '1px solid red'});
        flag = 1;
    }
    else {
        nameIn.css({'border-bottom': '1px solid green'});
        nameErr.innerHTML = '';
    }

});
phoneIn.blur(function () {
    var phone_number = $('#phone_number').val();
    var character = new Array(phone_number.length);
    var i,a , test = true;
    for (i=0, a = phone_number.length; i < a; i=i+1) {
        character[i] = phone_number[i];
        if (character[i] == "1" || character[i] == "2" || character[i] == "3" ||
            character[i] == "4"|| character[i] == "5" || character[i] == "6" ||
            character[i] == "7" || character[i] == "8" ||
            character[i] == "9" || character[i] == "0") {
            test = false;
        } else {
            test = true;
            break;
        }
    }

    if (!phone_number){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = 'We need to know your phone number!';
        flag = 1;
    }
    else if (test){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number shouldn\'t contain letters or spaces";
        flag = 1;
    }
    else if (phone_number.length < 11 || phone_number.length > 11){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number should be 11 number";
        flag = 1;
    }
    else {
        phoneIn.css({'border-bottom': '1px solid green'});
        phoneErr.innerHTML = "";
    }
});
emailIn.blur(function () {
    var email = $('#email').val();
    var findAt = email.search(/@/i);
    var findCom = email.search(/.com/i);

    if (!email){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "We need to know your e-mail address!";
        flag = 1;
    } else if (findAt == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain @";
        flag = 1;
    } else if (findCom == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain .com";
        flag = 1;
    } else {
        emailIn.css({'border-bottom': '1px solid green'});
        emailErr.innerHTML = "";
    }
});


$('#subm').click(function () {
    var name = $('#name').val(),
        phone_number = $('#phone_number').val(),
        email = $('#email').val(),
        password = $('#password').val();
    var flag = 0;

    /*************** name ***************/

    var character = new Array(name.length);
    var i,a , tester = false;
    for (i=0, a = name.length; i < a; i=i+1) {
        character[i] = name[i];
        if (character[i] == "1" || character[i] == "2" || character[i] == "3" ||
            character[i] == "4"|| character[i] == "5" || character[i] == "6" ||
            character[i] == "7" || character[i] == "8" ||
            character[i] == "9" || character[i] == "0"){
            tester = true;
        }
    }


    if (!name){
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "We need to know your name!";
        flag = 1;
    }
    else if(tester){
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "Name shouldn't contain numbers";
        flag = 1;
    }
    else if (name.length < 5){
        nameErr.innerHTML = 'Name should be more than 5 letters.';
        nameIn.css({'border-bottom': '1px solid red'});
        flag = 1;
    }
    else {
        nameIn.css({'border-bottom': '1px solid green'});
        nameErr.innerHTML = '';
    }

     /**********end name**********************/

    /***********phone*************************/

    var character = new Array(phone_number.length);
    var i,a , test = true;
    for (i=0, a = phone_number.length; i < a; i=i+1) {
        character[i] = phone_number[i];
        if (character[i] == "1" || character[i] == "2" || character[i] == "3" ||
            character[i] == "4"|| character[i] == "5" || character[i] == "6" ||
            character[i] == "7" || character[i] == "8" ||
            character[i] == "9" || character[i] == "0") {
            test = false;
        } else {
            test = true;
            break;
        }
    }

    if (!phone_number){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = 'We need to know your phone number!';
        flag = 1;
    }
    else if (test){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number shouldn\'t contain letters or spaces";
        flag = 1;
    }
    else if (phone_number.length < 11 || phone_number.length > 11){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number should be 11 number";
        flag = 1;
    }
    else {
        phoneIn.css({'border-bottom': '1px solid green'});
        phoneErr.innerHTML = "";
    }


    /**************end phone********************/

    /**************email************************/

    var findAt = email.search(/@/i);
    var findCom = email.search(/.com/i);

    if (!email){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "We need to know your e-mail address!";
        flag = 1;
    }
    else if (findAt == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain @";
        flag = 1;
    }
    else if (findCom == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain .com";
        flag = 1;
    }
    else {
        emailIn.css({'border-bottom': '1px solid green'});
        emailErr.innerHTML = "";
    }

    /**************end email**********************/

    /*************password************************/


    if (!password){
        passwordIn.css({'border-bottom': '1px solid red'});
        passwordErr.innerHTML = 'You need to write your password!';
        flag = 1;
    }

    /*************password************************/



    if ($("#company option:selected").val() === "" ) {
        $('#company').css({'border-bottom': '2px solid red'});
        flag = 1;
    }
    if (flag == 0){
        $("#name, #email, #phone_number , #company , #password" ).css( "border-bottom", "1px solid green" );
        $('#submitForm').submit();
    }
});





