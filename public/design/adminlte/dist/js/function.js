function check_all() {
    $('input[class="item_checkbox"]:checkbox').each(function () {
        if ($('input[class="check_all"]:checkbox:checked').length == 0){
            $(this).prop('checked' , false);
        } else {
            $(this).prop('checked' , true);
        }
    });
}
function delete_all() {
    $(document).on('click' , '.delBtn', function () {
        $num_checked = $('input[class="item_checkbox"]:checkbox:checked').length ;
        if ($num_checked > 0){
            $('.records').removeClass('hidden');
            $('.records_num').text($num_checked);
            $('.no_records').addClass('hidden');
        } else {
            $('.records').addClass('hidden');
            $('.records_num').text('');
            $('.no_records').removeClass('hidden');
        }
        $('#mutiDelete').modal('show');
    })
}

function sub() {
    $('#formSubmit').submit();
}
