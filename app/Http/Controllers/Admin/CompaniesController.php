<?php


namespace App\Http\Controllers\Admin;
use App\Company;
use App\DataTables\CompaniesDatatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CompaniesDatatable $company)
    {
        return $company->render('company.index' , ['title' => 'Company Controller']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(),[
           'name' => 'required',
           'tel' => 'required|numeric',
           'email' => 'unique:companies|required|email',
           'address' => 'required'
        ]);

        Company::create([
            'name' => request('name'),
            'tel' => request('tel'),
            'address' => request('address'),
            'email' => request('email')
        ]);

        session()->flash('added' , 'Company has been added');
        return redirect(aurl('companies'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('company.edit' , compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'name' => 'required',
            'tel' => 'required|numeric',
            'email' => 'required|email|unique:companies,email,'.$id,
            'address' => 'required'
        ]);

        $data = [
            'name' => request('name'),
            'tel' => request('tel'),
            'address' => request('address'),
            'email' => request('email')
        ];

        Company::where('id' , $id)->update($data);

        session()->flash('added' , 'Data has been Updated');
        return redirect(aurl('companies'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::find($id)->delete();
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('companies'));
    }
    public function multi_delete()
    {
        Company::destroy(request('item'));
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('companies'));
    }
}
