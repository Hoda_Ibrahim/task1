<?php


namespace App\Http\Controllers\Admin;
use App\DataTables\EmployeeDatatable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Company;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EmployeeDatatable $employee)
    {
        return $employee->render('user.index' , ['title' => 'Users Controller']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('user.create' , compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request() ,[
            'password' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'company_id' => $request->company
        ]);
        session()->flash('added' , 'Employee has been added');
        return redirect(aurl('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = User::find($id);
        $companies = Company::all();
        return view('user.edit' , ['companies' => $companies , 'employee' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request() ,[
            'password' => 'sometimes|nullable',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'phone' => 'required|numeric',
        ]);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'company_id' => $request->company,
        ];
        if (request()->has('password')){
            $data += [ 'password' => bcrypt($request->password) ];
        }

        User::where('id' , $id)->update($data);

        session()->flash('added' , 'Data has been updated');
        return redirect(aurl('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('users'));
    }
    public function multi_delete()
    {
        User::destroy(request('item'));
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('users'));
    }
}