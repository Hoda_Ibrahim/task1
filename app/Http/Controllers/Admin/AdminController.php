<?php

namespace App\Http\Controllers\Admin;
use App\DataTables\AdminDatatable;
use App\Http\Controllers\Controller;


//use App\Http\Middleware\AdminMiddle;
use App\Company;
use App\Admin;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdminDatatable $admin)
    {
        return $admin->render('admin.admins.index' , ['title' => 'Admin Controller']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        return view('admin.admins.create' , compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request() ,[
            'password' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'phone' => 'required|numeric',
        ]);

        Admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'company_id' => $request->company
        ]);
        session()->flash('added' , 'Admin has been added');
        return redirect(aurl('admin'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find($id);
        $companies = Company::all();
        return view('admin.admins.edit' , ['companies' => $companies , 'admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request() ,[
            'password' => 'sometimes|nullable',
            'name' => 'required',
            'email' => 'required|email|unique:admins,email,'.$id,
            'phone' => 'required|numeric',
        ]);
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'company_id' => $request->company,
        ];
        if (request()->has('password')){
            $data += [ 'password' => bcrypt($request->password) ];
        }

        Admin::where('id' , $id)->update($data);

        session()->flash('added' , 'Data has been updated');
        return redirect(aurl('admin'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Admin::find($id)->delete();
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('admin'));
    }
    public function multi_delete()
    {
        Admin::destroy(request('item'));
        session()->flash('added' , 'Data has been Deleted');
        return redirect(aurl('admin'));
    }
}
