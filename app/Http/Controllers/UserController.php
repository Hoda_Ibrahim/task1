<?php

namespace App\Http\Controllers;
use App\Company;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function register()
    {
        $companies = Company::all();
        return view('user.register' , compact('companies'));
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'company' => 'required'
        ]);

        $user = User::create([
            'name'  => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'company_id' => $request->company,
        ]);
        auth()->login($user);

        session()->flash('message' , 'Your data has been submitted');
        return redirect('home');

    }

    public function index()
    {
        return view('user.home');
    }

    public function login()
    {
        return view('user.login');
    }

    public function loginAction()
    {
        $rememberme = request('rememberme') ==1?true:false ;
        if(auth()->attempt(['email'=>request('email') , 'password'=> request('password')], $rememberme)){
            return redirect('home');
        } else {
            session()->flash('error' , 'Error Information');
            return redirect(url('login'));
        }
    }


    public function logout()
    {
        auth()->logout();
        return redirect('login');
    }
}
