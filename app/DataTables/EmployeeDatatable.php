<?php

namespace App\DataTables;
use App\User;
use Yajra\DataTables\Services\DataTable;

class EmployeeDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('edit', 'user.btn.edit')
            ->addColumn('delete', 'user.btn.delete')
            ->addColumn('checkbox', 'user.btn.checkbox')
            ->rawColumns([
                'edit' , 'delete' , 'checkbox'
            ]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return User::query();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->addAction(['width' => '80px'])
                    //->parameters($this->getBuilderParameters());
                    ->parameters([
                    'dom' => 'Blfrtip',
                    'lengthMenu' => [[10,25,50,100] , [10,25,50,'All Records']],
                    'buttons' => [
                        [
                            'text' => '<i class="fa fa-plus"></i>Add Employee',
                            'className' => 'btn btn-primary',
                            'action' => "function(){
                                    window.location.href = '".\URL::current()."/create';
                                }",
                        ],
                        ['extend' => 'print', 'className' => 'btn btn-primary' ,'text'=>"<i class='fa fa-print'> Print page</i> "],
                        ['extend' => 'csv', 'className' => 'btn btn-info' ,'text'=>" <i class='fa fa-file'> Export csv</i>"],
                        ['extend' => 'excel', 'className' => 'btn btn-success' ,'text'=>" <i class='fa fa-file'> Export excel</i>"],
                        ['extend' => 'reload', 'className' => 'btn btn-default' ,'text'=>" <i class='fa fa-refresh'></i>"],
                        [
                            'text' => '<i class="fa fa-trash"></i> Delete all',
                            'className' => 'btn btn-danger delBtn',
                        ],
                    ],
                    'initComplete' =>"
                                function () {
                                    this.api().columns([0,1,2,3,4,5]).every(function () {
                                        var column = this;
                                        var input = document.createElement(\"input\");
                                        $(input).appendTo($(column.footer()).empty())
                                        .on('keyup', function () {
                                            column.search($(this).val(), false, false, true).draw();
                                        });
                                    });
                                 }",
                 ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'name' => 'id',
                'data' => 'id',
                'title' => 'ID',
            ],[
                'name' => 'name',
                'data' => 'name',
                'title' => 'Admin name',
            ],[
                'name' => 'email',
                'data' => 'email',
                'title' => 'Admin email',
            ],[
                'name' => 'phone',
                'data' => 'phone',
                'title' => 'Admin phone',
            ],[
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => 'Created at',
            ],[
                'name' => 'updated_at',
                'data' => 'updated_at',
                'title' => 'Updated at',
            ],[
                'name' => 'edit',
                'data' => 'edit',
                'title' => 'Edit',
                'exportable' =>false,
                'printable' => false,
                'orderable' => false,
                'searchable' => false,

            ],[
                'name' => 'delete',
                'data' => 'delete',
                'title' => 'Delete',
                'exportable' =>false,
                'printable' => false,
                'orderable' => false,
                'searchable' => false,

            ],[
                'name' => 'checkbox',
                'data' => 'checkbox',
                'title' => '<input type="checkbox" onclick="check_all()" class="check_all">',
                'exportable' =>false,
                'printable' => false,
                'orderable' => false,
                'searchable' => false,

            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
